#-------------------------------------------------
#
# Project created by QtCreator 2014-07-26T22:12:22
#
#-------------------------------------------------

QT      += core gui
QT      += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RFVserial
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
